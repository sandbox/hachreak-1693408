<?php
/**
 * $attribute - attribute with the list of options
 * $theme_options - theme options
 *
 */

$option_image_preset = $theme_options['imagecache_preset'];
$options_html = "";
// get default option selected
$default_option_oid = $attribute->options[$attribute->default_option]->oid;
// get default image
$default_option_image = $attribute->options[$attribute->default_option]->option_image;

// output of all options
foreach($attribute->options as $option){
  // set active class for default option
  $active_class = "";
  if($default_option_oid == $option->oid) { $active_class = "active"; }

  $options_html .= '<span id="option-'.$option->nid.'-'.$option->aid.'-'.$option->oid.'" class="uc-attribute-options-option uc-attribute-options-option-'.$option->oid.' uc-attribute-options-option-'.str_replace(" ", "-", strtolower($option->name)).' '.$active_class.'">'.$option->name.'</span>';
}

// if there are a default image, print it
if($default_option_image):
?>
  <?php if($theme_options['link_to_node']): ?>
    <a href="<?php echo url('node/'.$attribute->options[$attribute->default_option]->nid) ?>">
  <?php endif; ?>
    <div class="uc-attribute-options-option-default"><?php echo theme('imagecache', $option_image_preset, $attribute->options[$attribute->default_option]->option_image->filepath, NULL, NULL, $option_image_attributes) ?></div>
  <?php if($theme_options['link_to_node']): ?>
     </a>
  <?php endif; ?>
<?php
endif;
?>

<div class="attribute-options"><?php echo $options_html; ?></div>
