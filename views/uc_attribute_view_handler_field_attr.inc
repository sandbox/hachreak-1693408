<?php
/**
 * @file
 * Attribute field declaration for views
 */

class uc_attribute_view_handler_field_attr extends views_handler_field {
  
  function query() {
    $aid = explode('_', $this->field);
    $this->aid = $aid[1];

    if (module_exists('uc_option_image')) {
      drupal_add_js(drupal_get_path('module', 'uc_attribute_view') . '/js/dinamyc_load_option_image.js');
    }
  }

  /**
   * Define options available for this field.
   */
  function option_definition() {
    $options = parent::option_definition();

    if (module_exists('uc_option_image')) {
      $options['imagecache_preset'] = array('default' => '');
      $options['link_to_node'] = array('default' => FALSE);
    }

    return $options;
  }

  /**
   * Build option configuration form.
   */
  function options_form(&$form, &$form_state) {
    // Get the parent's $form array.  
    parent::extra_options_form($form, $form_state);


    if (module_exists('uc_option_image')) {
      // If ImageCache module is found, add its presets as available options
      // for how to display the image.
      if (module_exists('imagecache')) {
        $raw_presets = imagecache_presets();
        $presets[''] = t('Default');
        foreach ($raw_presets as $preset_id => $preset_info) {
          $preset = $preset_info['presetname'];
          $presets[$preset] = $preset;
        }
        $form['imagecache_preset'] = array(
          '#type' => 'select',
          '#title' => t('ImageCache preset'),
          '#options' => $presets,
          '#default_value' => $this->options['imagecache_preset'],
        );
      }

      $form['link_to_node'] = array(
        '#title' => t('Link this image to its node'),
        '#type' => 'checkbox',
        '#default_value' => !empty($this->options['link_to_node']),
      );
    }
    return $form;
  }

  function render($values) {
    $attribute =  uc_attribute_load($this->aid, $values->nid, 'product');

    if (module_exists('uc_option_image')) {
      $data = array();
      if (module_exists('uc_option_image')) {
        foreach ($attribute->options as $key => $option) {
          $oi = uc_option_image_load($values->nid, $this->aid, $key);
          $attribute->options[$key]->option_image = $oi;

          // Render image. If ImageCache preset is specified, use it.
          if ($this->options['imagecache_preset']) {
            $url = imagecache_create_url($this->options['imagecache_preset'], $oi->filepath, $bypass_browser_cache = FALSE, $absolute = TRUE);
          }
        
          $data['images']['option-'. $values->nid .'-'. $this->aid .'-'. $key] = $url;
        }
      }
      
      drupal_add_js(array('ucav_ucoi' => $data), 'setting');
    }

    return theme('uc_attribute_view_field_attribute_options', $attribute, $this->options);
  }

}
