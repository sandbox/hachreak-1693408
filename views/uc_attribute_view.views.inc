<?php
/**
 * @file
 * View declaration.
 */


function uc_attribute_view_views_data() {

  $data['uc_product_options']['table']['group'] = t('Product Attributes');
  $data['uc_product_options']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
    'uc_product_attributes' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );
  
  $result = db_query("SELECT aid, name, description FROM {uc_attributes}");
  while ($row = db_fetch_object($result)) {
    $data['uc_product_options']['attr_'. $row->aid] = array(
      'title' => 'Attribute: '. $row->name,
      'help' => 'Attribute desc: '. $row->description,
      'filter' => array(
        'handler' => 'uc_attribute_view_handler_filter_attr',
      ),
      'field' => array(
        'handler' => 'uc_attribute_view_handler_field_attr',
        'click sortable' => FALSE,
      ),
    );  
  }
 
  return $data;
  
}

function uc_attribute_view_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'uc_attribute_view') . '/views',
    ),
    'handlers' => array(
      'uc_attribute_view_handler_filter_attr' => array('parent' => 'views_handler_filter_in_operator', ),
      'uc_attribute_view_handler_field_attr' => array('parent' => 'views_handler_field', ),
    ),
  );
}
