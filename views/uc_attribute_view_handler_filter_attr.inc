<?php
/**
 * @file
 * Attribute filter declaration for views
 */

class uc_attribute_view_handler_filter_attr extends views_handler_filter_in_operator {
  function get_value_options() {
    if (!isset($this->value_options)) {
      $aid = explode('_', $this->field);
      $aid = $aid[1];
      
      $this->value_title = t('Options');
      $result = db_query("SELECT name, oid FROM {uc_attribute_options} WHERE aid = %d ORDER BY ordering", $aid);

      if (function_exists("i18nstrings")) {
        while ($row = db_fetch_object($result)) {
          $options[$row->oid] = i18nstrings('uc_attribute:option:'. $row->oid .':name', $row->name);
        }
      }
      else {
        while ($row = db_fetch_object($result)) {
          $options[$row->oid] = $row->name;
        }
      }

      if (count($options) == 0) {
        $options[] = t('No options found.');
      }
      $this->value_options = $options;
    }
  }
  
  function query() {
    $aid = explode('_', $this->field);
    $aid = $aid[1];
        
    $this->ensure_my_table();
    $this->real_field = 'oid';
    $field = "$this->table_alias.$this->real_field";

    if ($this->operator == 'not in') {
      $not = 'NOT';
    }

    if (!is_array($this->value)) {
      $this->value = array($this->value);
    }
    
    $orv = "";
    foreach ($this->value as &$value) {
      if ($this->options['type'] == 'textfield') {
        $optval = $value;  
      } 
      else {
        $optval = db_result(db_query('SELECT name FROM {uc_attribute_options} WHERE oid = %d', $value));  
      }

      if ($orv != "") $orv .= " OR ";
      $orv .= "$field $not LIKE '$value'";
    }

    $this->query->add_where($this->options['group'], $orv);
  }

}
