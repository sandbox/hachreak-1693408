Drupal.behaviors.uc_attribute_view_behaviour = function (context) {
  $('.uc-attribute-options-option').click(function(){
    // remove previuos selection class
    $(this).parent().find('.uc-attribute-options-option').removeClass('active');
    // add active class
    $(this).addClass('active');
    // find main image
    var image = $(this).parent().parent().find('.uc-attribute-options-option-default');
    // get attribute id
    var oid = $(this).attr('id');
    // replace main image
    image.find('img').attr('src', Drupal.settings.ucav_ucoi.images[oid]);
  });
};
